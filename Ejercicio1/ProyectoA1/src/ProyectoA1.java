
import java.util.Scanner;

public class ProyectoA1 {

    
    public static void main(String[] args) {
        long inicio = System.currentTimeMillis();
        int n;
        double[] arr = new double[100];
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the number of elements (1 to 100): ");
        n = scan.nextInt();

        for (int i = 0; i < n; ++i) {
          System.out.print("Enter number" + (i + 1) + ": ");
          arr[i] = scan.nextDouble();
        }

      
        for (int i = 1; i < n; ++i) {
          if (arr[0] < arr[i]) {
            arr[0] = arr[i];
          }
        }

        System.out.println("Largest element = " + arr[0]);
        long end = System.currentTimeMillis(); 
        long total = end-inicio;
        System.out.println("Tiempo de ejecucion: "+total+" milisegundos");
    }
    
}
