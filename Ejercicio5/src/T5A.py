#Paradigma: orientado a objetos
#Metodo: Interpretado
#Traductor: Python

import time

class Node:
    def __init__(self, key):
        self.key = key
        self.left = None
        self.right = None

class BinaryTree:
    def __init__(self):
        self.root = None

def traverseTree(node):
    if node:
        traverseTree(node.left)
        print(node.key, end=" ")
        traverseTree(node.right)

if __name__ == '__main__':
    
    start = time.perf_counter()

    # create an object of BinaryTree
    tree = BinaryTree()

    # create nodes of the tree
    tree.root = Node(1)
    tree.root.left = Node(2)
    tree.root.right = Node(3)
    tree.root.left.left = Node(4)

    print("\nBinary Tree: ", end="")
    traverseTree(tree.root)
    
    end = time.perf_counter()
    totalTime = round((end - start) * 1000, 15)
    print("\nTiempo de ejecucion en milisegundos: ", totalTime)

