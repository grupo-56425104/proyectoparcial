// Paradigma de programación utilizado: Estructurada
// Método de implementación: Compilado 
// Tipo de traductor: C
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct Node {
    int key;
    struct Node *left;
    struct Node *right;
};

struct BinaryTree {
    struct Node *root;
};

void traverseTree(struct Node *node) {
    if (node != NULL) {
        traverseTree(node->left);
        printf(" %d", node->key);
        traverseTree(node->right);
    }
}

int main(void) {

    clock_t startTime = clock();
    // create an object of BinaryTree
    struct BinaryTree tree;

    // create nodes of the tree
    tree.root = (struct Node*)malloc(sizeof(struct Node));
    tree.root->key = 1;
    tree.root->left = (struct Node*)malloc(sizeof(struct Node));
    tree.root->right = (struct Node*)malloc(sizeof(struct Node));
    tree.root->left->key = 2;
    tree.root->right->key = 3;
    tree.root->left->left = (struct Node*)malloc(sizeof(struct Node));
    tree.root->left->left->key = 4;
    tree.root->left->left->left = NULL;
    tree.root->left->left->right = NULL;
    tree.root->left->right = NULL;
    tree.root->right->left = NULL;
    tree.root->right->right = NULL;

    printf("\nBinary Tree: ");
    traverseTree(tree.root);

    clock_t endTime = clock();

    double totalTime = ((double)(endTime - startTime)) / CLOCKS_PER_SEC * 1000;
    printf("\nTiempo de ejecucion en milisegundos: %.15f", totalTime);

    return 0;
}