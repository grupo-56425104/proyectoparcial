#include <stdio.h>
#include <time.h>

int main() {

   clock_t startTime = clock();

  char op;
  double first, second;
  printf("Enter an operator (+, -, *, /): ");
  scanf("%c", &op);
  printf("Enter two operands: ");
  scanf("%lf %lf", &first, &second);

  switch (op) {
    case '+':
      printf("%.1lf + %.1lf = %.1lf", first, second, first + second);
      break;
    case '-':
      printf("%.1lf - %.1lf = %.1lf", first, second, first - second);
      break;
    case '*':
      printf("%.1lf * %.1lf = %.1lf", first, second, first * second);
      break;
    case '/':
      printf("%.1lf / %.1lf = %.1lf", first, second, first / second);
      break;
    // operator doesn't match any case constant
    default:
      printf("Error! operator is not correct");
  }
  
    clock_t endTime = clock();

    double totalTime = ((double)(endTime - startTime)) / CLOCKS_PER_SEC * 1000;
    printf("\nTiempo de ejecucion en milisegundos: %.15f", totalTime);


  return 0;
}