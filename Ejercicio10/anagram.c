#include <stdio.h>
#include <string.h>
#include <time.h>

int check_anagram(char [], char []);

int main()
{
    clock_t startTime = clock();

    char a[] = "race";
    char b[] = "care";

    if (check_anagram(a, b) == 1)
        printf("%s and %s are anagrams\n", a, b);
    else
        printf("%s and %s are not anagrams\n", a, b);

    clock_t endTime = clock();

    double totalTime = ((double)(endTime - startTime)) / CLOCKS_PER_SEC * 1000;
    printf("\nTiempo de ejecucion en milisegundos: %.15f", totalTime);

    return 0;
}

int check_anagram(char a[], char b[])
{
    int first[26] = {0}, second[26] = {0}, c = 0;

    // Calculating frequency of characters of first string
    while (a[c] != '\0')
    {
        first[a[c] - 'a']++;
        c++;
    }

    c = 0;

    while (b[c] != '\0')
    {
        second[b[c] - 'a']++;
        c++;
    }

    // Comparing frequency of characters
    for (c = 0; c < 26; c++)
    {
        if (first[c] != second[c])
            return 0;
    }

    return 1;
}