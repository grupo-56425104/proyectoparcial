#Paradigma: Orientado a objetos
#Metodo de implementacion: Clases y objetos
#Traductor: Python
import time

MAX_EDGES = 8

# structure to keep track of edges
class Edge:
    def __init__(self, src, dest):
        self.src = src
        self.dest = dest

# number of vertices and edges
vertices = 0
edges = 0

# list to store all edges
edge = []

if __name__ == '__main__':
    start = time.perf_counter()

    # create graph
    vertices = 5
    edges = 8

    edge.append(Edge(1, 2)) # edge 1---2
    edge.append(Edge(1, 3)) # edge 1---3
    edge.append(Edge(1, 4)) # edge 1---4
    edge.append(Edge(2, 4)) # edge 2---4
    edge.append(Edge(2, 5)) # edge 2---5
    edge.append(Edge(3, 4)) # edge 3---4
    edge.append(Edge(3, 5)) # edge 3---5
    edge.append(Edge(4, 5)) # edge 4---5

    # print graph
    for i in range(edges):
        print(f"{edge[i].src} - {edge[i].dest}")

    end = time.perf_counter()
    totalTime = round((end - start) * 1000, 15)
    print("\nTiempo de ejecucion en milisegundos: ", totalTime)




