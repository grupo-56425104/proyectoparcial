// Paradigma de programación utilizado: Estructurada
// Método de implementación: Compilado
// Tipo de traductor utilizado: C

#include <stdio.h>
#include <time.h>

#define MAX_EDGES 8

// structure to keep track of edges
struct Edge {
int src, dest;
};

// number of vertices and edges
int vertices, edges;

// array to store all edges
struct Edge edge[MAX_EDGES];

int main() {

clock_t start = clock();

// create graph
vertices = 5;
edges = 8;

edge[0].src = 1; // edge 1---2
edge[0].dest = 2;

edge[1].src = 1; // edge 1---3
edge[1].dest = 3;

edge[2].src = 1; // edge 1---4
edge[2].dest = 4;

edge[3].src = 2; // edge 2---4
edge[3].dest = 4;

edge[4].src = 2; // edge 2---5
edge[4].dest = 5;

edge[5].src = 3; // edge 3---4
edge[5].dest = 4;

edge[6].src = 3; // edge 3---5
edge[6].dest = 5;

edge[7].src = 4; // edge 4---5
edge[7].dest = 5;

// print graph
for(int i = 0; i < edges; i++) {
printf("%d - %d\n", edge[i].src, edge[i].dest);
}

clock_t end = clock();
double totalTime = ((double)(end - start)) / CLOCKS_PER_SEC * 1000;
printf("\nTiempo de ejecucion en milisegundos: %.15f", totalTime);

return 0;
}

