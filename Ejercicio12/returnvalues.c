#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>

char** name(){
char** nom = malloc(2 * sizeof(char*));
nom[0] = malloc(strlen("John") + 1);
nom[1] = malloc(strlen("Armin") + 1);
strcpy(nom[0], "John");
strcpy(nom[1], "Armin");
return nom;
}

int main() {
clock_t startTime = clock();
char** nom = name();
printf("%s %s\n", nom[0], nom[1]);
printf("[%s, %s]\n", nom[0], nom[1]);

clock_t endTime = clock();

double totalTime = ((double)(endTime - startTime) / CLOCKS_PER_SEC) * 1000;
printf("Tiempo de ejecucion: %.15f milisegundos\n", totalTime);

free(nom[0]);
free(nom[1]);
free(nom);

return 0;
}