import time

def name():
    return "John","Armin"

# print the tuple with the returned values
print(name())

# get the individual items
name_1, name_2 = name()
print(name_1, name_2)

start = time.perf_counter()

print("This time is being calcpulated")

end = time.perf_counter()

print(end - start)