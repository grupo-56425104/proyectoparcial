// Paradigma de Programación: Estructurado
// Método de Implementación del Lenguaje: Compilado
// Tipo de Traductor: Java
import java.util.LinkedList;

class LinkedList2 {
  public static void main(String[] args){
    long start = System.nanoTime();

    // create a linked list using the LinkedList class
    LinkedList<String> animals = new LinkedList<>();

    // Add elements to LinkedList
    animals.add("Dog");

    // add element at the beginning of linked list
    animals.addFirst("Cat");

    // add element at the end of linked list
    animals.addLast("Horse");
    System.out.println("LinkedList: " + animals);

    // access first element
    System.out.println("First Element: " + animals.getFirst());

    // access last element
    System.out.println("Last Element: " + animals.getLast());

    long end = System.nanoTime();
    double totalTime = (double)(end - start) / 1000000;
    System.out.printf("\nTiempo de ejecucion: %.15f milisegundos\n", totalTime);
    }
  }



