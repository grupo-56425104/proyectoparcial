#paradigma: orientado a objetos
#metodo: Interpretado
#traductor: python

import time

class Node:
    def __init__(self, value):
        self.value = value
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

# create an object of LinkedList
linkedList = LinkedList()

# assign values to each linked list node
linkedList.head = Node(1)
second = Node(2)
third = Node(3)

# connect each node of linked list to next node
linkedList.head.next = second
second.next = third

startTime = time.perf_counter()

# printing node-value
print("LinkedList: ", end='')
current = linkedList.head
while current != None:
    print(current.value, end=' ')
    current = current.next

endTime = time.perf_counter()

totalTime = round((endTime - startTime) * 1000, 15)
print("\nTiempo de ejecucion en milisegundos: ", totalTime)


