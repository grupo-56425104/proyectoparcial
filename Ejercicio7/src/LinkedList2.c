// Paradigma de programación: Estructurada
// Método de implementación: Compilado
// Tipo de traductor: c

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

struct node {
char data[10];
struct node *next;
};

struct linked_list {
struct node *head;
int size;
};

void add_node(struct linked_list *list, char *data) {
struct node *new_node = (struct node *)malloc(sizeof(struct node));
strcpy(new_node->data, data);
new_node->next = list->head;
list->head = new_node;
list->size++;
}

void add_node_last(struct linked_list *list, char *data) {
struct node *new_node = (struct node *)malloc(sizeof(struct node));
strcpy(new_node->data, data);
new_node->next = NULL;

struct node *temp = list->head;
if (temp == NULL) {
list->head = new_node;
list->size++;
return;
}

while (temp->next != NULL) {
temp = temp->next;
}
temp->next = new_node;
list->size++;
}

char *get_first(struct linked_list *list) {
return list->head->data;
}

char *get_last(struct linked_list *list) {
struct node *temp = list->head;
while (temp->next != NULL) {
temp = temp->next;
}
return temp->data;
}

int main(void) {
clock_t start = clock();

struct linked_list *animals = (struct linked_list *)malloc(sizeof(struct linked_list));
animals->head = NULL;
animals->size = 0;

add_node(animals, "Dog");
add_node(animals, "Cat");
add_node_last(animals, "Horse");

printf("LinkedList: ");
struct node *temp = animals->head;
while (temp != NULL) {
printf("%s ", temp->data);
temp = temp->next;
}
printf("\n");

printf("First Element: %s\n", get_first(animals));
printf("Last Element: %s\n", get_last(animals));


clock_t end = clock();
double executionTime = (double)(end - start) / CLOCKS_PER_SEC * 1000;
printf("\nTiempo de ejecucion en milisegundos: %.15f", executionTime);


return 0;
}