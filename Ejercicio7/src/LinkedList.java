//Paradigma: orientado aobjetos
//Metodo: compilado
//Traductor: java

class LinkedList {

  // create an object of Node class
  // represent the head of the linked list
  Nodee head;

  public static void main(String[] args) {

    long startTime = System.nanoTime();

    // create an object of LinkedList
    LinkedList linkedList = new LinkedList();

    // assign values to each linked list node
    linkedList.head = new Nodee(1);
    Nodee second = new Nodee(2);
    Nodee third = new Nodee(3);

    // connect each node of linked list to next node
    linkedList.head.next = second;
    second.next = third;

    // printing node-value
    System.out.print("LinkedList: ");
    while (linkedList.head != null) {
      System.out.print(linkedList.head.value + " ");
      linkedList.head = linkedList.head.next;
    }

    long endTime = System.nanoTime();

    double totalTime = (double)(endTime - startTime) / 1000000;
    System.out.printf("\nTiempo de ejecucion: %.15f milisegundos\n", totalTime);
  }
}

  