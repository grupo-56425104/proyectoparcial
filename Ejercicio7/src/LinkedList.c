//paradigma: estructurado
//metodo:compilado
//traductor: c

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct Nodee {
    int value;
    struct Nodee* next;
};

struct LinkedList {
    struct Nodee* head;
};

int main() {

    clock_t startTime = clock();
    // create an object of LinkedList
    struct LinkedList linkedList;

    // assign values to each linked list node
    linkedList.head = (struct Nodee*)malloc(sizeof(struct Nodee));
    linkedList.head->value = 1;
    struct Nodee* second = (struct Nodee*)malloc(sizeof(struct Nodee));
    second->value = 2;
    struct Nodee* third = (struct Nodee*)malloc(sizeof(struct Nodee));
    third->value = 3;

    // connect each node of linked list to next node
    linkedList.head->next = second;
    second->next = third;
    third->next = NULL;

    // printing node-value
    printf("LinkedList: ");
    struct Nodee* current = linkedList.head;
    while (current != NULL) {
        printf("%d ", current->value);
        current = current->next;
    }

    clock_t endTime = clock();

    double totalTime = ((double)(endTime - startTime)) / CLOCKS_PER_SEC * 1000;
    printf("\nTiempo de ejecucion en milisegundos: %.15f", totalTime);

    // free memory
    free(linkedList.head);
    free(second);
    free(third);

    return 0;
}
