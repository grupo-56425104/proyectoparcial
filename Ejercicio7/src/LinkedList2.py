#Paradigma: orientado a objetos
#Metodo: Interpretado
#Traductor: Python

import time

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None
        self.size = 0

    def add_node(self, data):
        new_node = Node(data)
        new_node.next = self.head
        self.head = new_node
        self.size += 1

    def add_node_last(self, data):
        new_node = Node(data)
        new_node.next = None

        temp = self.head
        if temp is None:
            self.head = new_node
            self.size += 1
            return

        while temp.next is not None:
            temp = temp.next

        temp.next = new_node
        self.size += 1

    def get_first(self):
        return self.head.data

    def get_last(self):
        temp = self.head
        while temp.next is not None:
            temp = temp.next

        return temp.data

animals = LinkedList()

start_time = time.time()

animals.add_node("Dog")
animals.add_node("Cat")
animals.add_node_last("Horse")

print("LinkedList: ", end="")
temp = animals.head
while temp is not None:
    print(temp.data, end=" ")
    temp = temp.next
print()

print("First Element: ", animals.get_first())
print("Last Element: ", animals.get_last())

end_time = time.time()

totalTime = round((end_time - start_time) * 1000, 15)
print("\nTiempo de ejecucion en milisegundos: ", totalTime)

