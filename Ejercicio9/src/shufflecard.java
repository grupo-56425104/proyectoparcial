public class shufflecard {
    public static void main(String[] args) {

        long startTime = System.nanoTime();

        String[] SUITS = {
            "Clubs", "Diamonds", "Hearts", "Spades"
        };

        String[] RANKS = {
            "1","2", "3", "4", "5", "6", "7", "8", "9", "10",
            "11", "12", "13", "14"
        };

        // initialize deck
        int n = SUITS.length * RANKS.length;
        String[] deck = new String[n];
        for (int i = 0; i < RANKS.length; i++) {
            for (int j = 0; j < SUITS.length; j++) {
                deck[SUITS.length*i + j] = RANKS[i] + " of " + SUITS[j];
            }
        }

        // shuffle
        for (int i = 0; i < n; i++) {
            int r = i + (int) (Math.random() * (n-i));
            String temp = deck[r];
            deck[r] = deck[i];
            deck[i] = temp;
        }

        // print shuffled deck
        for (int i = 0; i < 5; i++) {
            System.out.println(deck[i]);
        }

        long inicio = System.currentTimeMillis();
         
        long fin = System.currentTimeMillis();
         
        double tiempo = (double) ((fin - inicio)/1000);
         
        System.out.println(tiempo +" segundos");

        long endTime = System.nanoTime();

        double totalTime = (double)(endTime - startTime) / 1000000;
        System.out.printf("\nTiempo de ejecucion: %.15f milisegundos\n", totalTime);
    }

}