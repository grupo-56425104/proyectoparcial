#include <time.h>
#include <stdio.h>

int main() {
    clock_t inicio = clock();
    char str[1000], ch;
    int count = 0;

    printf("Enter a string: ");
    fgets(str, sizeof(str), stdin);

    printf("Enter a character to find its frequency: ");
    scanf("%c", &ch);

    for (int i = 0; str[i] != '\0'; ++i) {
        if (ch == str[i])
            ++count;
    }

    printf("Frequency of %c = %d", ch, count);
    clock_t fin = clock();
    double tiempo_ejec = (double)(fin - inicio) / CLOCKS_PER_SEC*1000;
    printf("\ntiempo de ejecucion: %f milliseconds\n", tiempo_ejec);
    return 0;
}