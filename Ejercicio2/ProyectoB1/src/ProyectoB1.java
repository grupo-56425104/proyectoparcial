import java.util.Scanner;

public class ProyectoB1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        String oracion;
        char det;
        int count = 0;
        Scanner in = new Scanner(System.in);
        System.out.println("Ingrese un String ");
        oracion = in.nextLine();
        Scanner in0 = new Scanner(System.in);
        System.out.println("Ingrese un caracter para determinar su frecuencia ");
        det = in0.next().charAt(0);
        for(int i = 0;i < oracion.length();i++){
            if(oracion.charAt(i)==det){
                count++;
            }
        }
        System.out.println("La frecuencia de la letra "+det+" es: "+count);
        long end = System.currentTimeMillis();
        double executionTime = (double)(end - start);
        System.out.println("Tiempo de ejecucion: " + executionTime + " milisegundos");
    }
    
}
