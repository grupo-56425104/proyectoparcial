import time
start = time.time()
def reverse(string):
    if len(string) == 0:
        return string
    else:
        return reverse(string[1:]) + string[0]
a = str(input("Enter the string to be reversed: "))
print(reverse(a))
print("The time used to execute this is given below")
end = time.time()
print((end - start)*1000)