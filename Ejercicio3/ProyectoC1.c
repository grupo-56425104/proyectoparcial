#include <time.h>
#include <stdio.h>
void reverseSentence();
int main() {
    clock_t inicio = clock();
    printf("Enter a sentence: ");
    //funcion como en python
    reverseSentence();
    clock_t fin = clock();
    double tiempo_ejec = (double)(fin - inicio) / CLOCKS_PER_SEC*1000;
    printf("\ntiempo de ejecucion: %f milliseconds\n", tiempo_ejec);
    return 0;
}

void reverseSentence() {
    char c;
    scanf("%c", &c);
    if (c != '\n') {
        reverseSentence();
        printf("%c", c);
        

    }
}