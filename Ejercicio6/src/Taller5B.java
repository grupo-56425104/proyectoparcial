/*
Paradigma: Estructurado
Implementacion: Compilacion
Traductor: Java
*/


public class Taller5B {

    public static void main(String[] args) {
        long start = System.nanoTime();
        String str = "This website is awesome.";
        char ch = 'e';
        int frequency = 0;

        for(int i = 0; i < str.length(); i++) {
            if(ch == str.charAt(i)) {
                ++frequency;
            }
        }

        System.out.println("Frequency of " + ch + " = " + frequency);
        long end = System.nanoTime();
        double totalTime = (double)(end - start) / 1000000;
        System.out.printf("\nTiempo de ejecucion: %.15f milisegundos\n", totalTime);

    }
    
}
