#Paradigma: Orientado a objetos
#Metodo: Interpretado
#Traductor: Python

import time

start = time.perf_counter()
str = "This website is awesome."
ch = 'e'
frequency = 0

for i in range(len(str)):
    if ch == str[i]:
        frequency += 1

print(f"Frequency of {ch} = {frequency}")
end = time.perf_counter()
totalTime = round((end - start) * 1000, 15)
print("\nTiempo de ejecucion en milisegundos: ", totalTime)

