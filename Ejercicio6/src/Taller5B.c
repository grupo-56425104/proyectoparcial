/*
Paradigma de programación: Imperativo
Método de implementación: Procedural
Tipo de traductor utilizado: C
*/

#include <stdio.h>
#include <time.h>
#include <string.h>

int main() {
clock_t start = clock();
char str[] = "This website is awesome.";
char ch = 'e';
int frequency = 0;
int len = strlen(str);
for(int i = 0; i < len; i++) {
    if(ch == str[i]) {
    frequency++;
    }
}

printf("Frequency of %c = %d\n", ch, frequency);
clock_t end = clock();
double totalTime = ((double)(end - start)) / CLOCKS_PER_SEC * 1000;
printf("\nTiempo de ejecucion en milisegundos: %.15f", totalTime);
return 0;
}