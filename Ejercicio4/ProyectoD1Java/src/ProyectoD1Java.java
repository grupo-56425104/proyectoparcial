
import java.util.Scanner;






public class ProyectoD1Java {
    
    

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        
        class Student {
            String name;
            int roll;
            float marks;
        }
        
        Scanner in = new Scanner(System.in);
        Student s = new Student();
        System.out.println("Enter information:");
        System.out.print("Enter name: ");
        s.name = in.nextLine();

        System.out.print("Enter roll number: ");
        s.roll = in.nextInt();

        System.out.print("Enter marks: ");
        s.marks = in.nextFloat();
        System.out.println("Displaying Information:");
        System.out.println("Name: " + s.name);
        System.out.println("Roll number: " + s.roll);
        System.out.println("Marks: " + s.marks);

        long end = System.currentTimeMillis();
        double executionTime = (double)(end - start);
        System.out.println("Tiempo de ejecucion: " + executionTime + " milisegundos");
    }
    
}
